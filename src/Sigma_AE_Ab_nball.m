function [xc, r_ball] = Sigma_AE_Ab_nball(A_min,A_max,b_min,b_max,xc) 
% Sigma_AE_Ab_nball For a given interval linear system of equations 
% Ax = b, for all A in [A_min,A_max], exists b in [b_min,b_max], computes 
% an inner approximation of the largest inscribed n-ball 
% B = {x | ||x-xc|| <= r} in the corresponding set of x.
%
%   [xc, r] = Sigma_AE_Ab_nball(A_min,A_max,b_min,b_max,xc) returns
%   the largest n-ball r value centered at the given xc.
%
%   [xc, r] = Sigma_AE_Ab_nball(A_min,A_max,b_min,b_max,[]) returns
%   the largest n-ball r value and corresponding center xc.

    assert(size(A_min,1) >= size(A_min,2));  
    assert(size(A_min,1) == size(A_max,1));   
    assert(size(A_min,2) == size(A_max,2)); 
    assert(size(b_min,1) == size(b_max,1)); 
    assert(size(b_max,2) == size(b_max,2)); 
    assert(size(A_min,1) == size(b_min,1));   
    assert(isempty(xc) || size(A_min,2) == size(xc,1)); 
    assert(all(A_max >= A_min, 'all'));  
    assert(all(b_max >= b_min, 'all'));
    
    A_mid = (A_max + A_min) / 2;
    A_rad = (A_max - A_min) / 2;
    b_rad = (b_max - b_min) / 2;
    b_mid = (b_max + b_min) / 2;
        
    if ~isempty(xc)
        %% Check if xc is a tolerance solution 
        % (J. Rohn, Solvability of systems of interval linear equations and 
        % inequalities, Springer US, Boston, MA, 2006, pp. 35-77 (2006).) 
        if any(abs(A_mid*xc - b_mid) > -A_rad*abs(xc) + b_rad)
            error("Error: xc not a tolerance solution");
        end
    
        %% Compute r of inscribed n-ball centered at xc
        r_list = nan(size(A_mid,1),1);        
        for i=1:size(A_mid,1)
            sup_ni = norm(sqrt(max(A_min(i,:).^2,A_max(i,:).^2)));
            r_list(i) = min([(A_min(i,:)*xc - b_min(i)) / sup_ni,...
                (A_max(i,:)*xc - b_min(i)) / sup_ni,...
                (-A_min(i,:)*xc + b_max(i)) / sup_ni,...
                (-A_max(i,:)*xc + b_max(i)) / sup_ni]);
        end
        r_ball = max(0,min(r_list));
    else
        %% Compute r of inscribed n-ball with variable xc
        [xc, r_ball] = nball_variable_center(A_min,A_max,b_min,b_max);
    end
end
