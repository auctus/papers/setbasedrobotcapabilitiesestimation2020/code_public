close all;
clear;
clc;

%% Add paths
addpath("plotting");
addpath("src");

%% Interval linear system
A_mid = [0.8947   0.6707   0.2409; 0.3348   0.3899   0.6958];
A_rad = [0.01, 0.01, 0.01; 0.01, 0.01, 0.01];
A_min = A_mid - A_rad;
A_max = A_mid + A_rad;

x_mid = [0; 0; 0];
x_rad = [1; 1; 1];
x_min = x_mid - x_rad;
x_max = x_mid + x_rad;

b_mid = [10.5; -2.0; 5.5];
b_rad = [84.5; 22.0; 27.5];
b_min = b_mid - b_rad;
b_max = b_mid + b_rad;

bc = [0;0];
xc = [0;0];

%% Sigma_AE_Ab inner approximations

[xc_ball, r_ball] = Sigma_AE_Ab_nball(A_min',A_max',b_min,b_max,xc);
[xc_ball_var, r_ball_var] = Sigma_AE_Ab_nball(A_min',A_max',b_min,b_max,[]);
[xc_cube, r_cube] = Sigma_AE_Ab_ncube(A_min',A_max',b_min,b_max,xc);
[xc_cube_var, r_cube_var] = Sigma_AE_Ab_ncube(A_min',A_max',b_min,b_max,[]);
[polytope_vertices] = Sigma_AE_Ab_polytope(A_min',A_max',b_min,b_max);
[polytope_vertices_mid] = Sigma_AE_Ab_polytope(A_mid',A_mid',b_min,b_max);

figure;
plot_inner_approx(polytope_vertices, [], [], [],'k','b');
plot_inner_approx(polytope_vertices_mid, [], [], [], 'k','none');
plot_inner_approx([], [], r_ball, xc_ball, 'k','g');
plot_inner_approx([], [], r_ball_var, xc_ball_var, 'k','none','--');
plot_inner_approx([], r_cube, [], xc_cube, 'k','r');
plot_inner_approx([], r_cube_var, [], xc_cube_var, 'k','none','--');
xlabel('$x_1$','interpreter','latex','FontSize', 20);
ylabel('$x_2$','interpreter','latex','FontSize', 20);
title('$\Sigma_{\forall\exists}([\bf A],[\bf b])$','interpreter','latex','FontSize', 20);

%% Omega_AE_Ax inner approximations

[bc_cube, r_cube, r_polytope_cube] = Omega_AE_Ax_ncube(A_min,A_max,x_min,x_max,bc);
[bc_cube_var, r_cube_var, r_polytope_cube_var] = Omega_AE_Ax_ncube(A_min,A_max,x_min,x_max,[]);
[bc_ball, r_ball, r_polytope_ball] = Omega_AE_Ax_nball(A_min,A_max,x_min,x_max,bc);
[bc_ball_var, r_ball_var, r_polytope_ball_var] = Omega_AE_Ax_nball(A_min,A_max,x_min,x_max,[]);
[polytope_vertices, r_polytope] = Omega_AE_Ax_polytope(A_min,A_max,x_min,x_max);
[polytope_vertices_mid, r_polytope_mid] = Omega_AE_Ax_polytope(A_mid,A_mid,x_min,x_max);

figure;
plot_inner_approx(polytope_vertices, [], [], [],'k','b');
plot_inner_approx(polytope_vertices_mid, [], [], [], 'k','none');
plot_inner_approx([], [], r_ball, bc_ball, 'k','g');
plot_inner_approx([], [], r_ball_var, bc_ball_var, 'k','none','--');
plot_inner_approx([], r_cube, [], bc_cube, 'k','r');
plot_inner_approx([], r_cube_var, [], bc_cube_var, 'k','none','--');
xlabel('$b_1$','interpreter','latex','FontSize', 20);
ylabel('$b_2$','interpreter','latex','FontSize', 20);
title('$\Omega_{\forall\exists}([\bf A],[\bf x])$','interpreter','latex','FontSize', 20);